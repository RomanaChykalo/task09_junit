package com.chykalo;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileReader {
    int[] intArrayOfNumbers;

    public FileReader() {
        intArrayOfNumbers = readNumbersFromFile();
    }

    public int[] readNumbersFromFile() {
        List<String> listFromFile = new ArrayList<>();
        try {
            File file = new File(SimpleOperation.class.getResource("/numbers.txt").toURI());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                listFromFile.add(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        String numbers = listFromFile.toString();
        String[] numbersArray = numbers.substring(1, numbers.length() - 1).replace(",", "").split(" ");

        intArrayOfNumbers = Arrays.stream(numbersArray)
                .mapToInt(Integer::parseInt)
                .toArray();
        return intArrayOfNumbers;
    }

    public void createFile(String path) {
        Path newFilePath = Paths.get(path);
        try {
            Files.createFile(newFilePath);
        } catch (IOException e) {
            System.out.println("File already exists!!!");
        }
    }


}

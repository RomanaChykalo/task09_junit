package com.chykalo;

public class SimpleOperation {
    FileReader fileReader;
    int[] ints;

    public SimpleOperation(FileReader fileReader) {
        this.fileReader = fileReader;
        this.ints = fileReader.readNumbersFromFile();
    }

    //i and k - it's an index of elements from array which we read from a file
    public int multiplication(int i, int k) {
        int multi = ints[k] * ints[i];
        return multi;
    }

    public int getSumOfAllNumbersBeforeElementNumber(int k) {
        int sum = 0;
        int sum1 = 0;
        for (int i = 0; i < k; i++) {
            sum = ints[i] + ints[i + 1];
            sum1 += ints[i];
        }
        return sum1;
    }

    public int divideNumbers(int i, int k) {
        int divide = 0;
        try {
            divide = i / k;
        } catch (ArithmeticException e) {
            System.out.println("You can't divide by zero.");
        }
        return divide;
    }

    public boolean validate(int primeNumber) {
        for (int i = 8; i >= (primeNumber * 2); i++) {
            if (i % primeNumber == 0) {
                return false;
            }
        }
        return true;
    }
}

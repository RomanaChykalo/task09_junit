package com.chykalo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        TestSimpleOperation.class,
        TestFileReader.class,
        TestValidateMethod.class
})

public class JunitTestSuite {
}

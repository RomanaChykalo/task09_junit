package com.chykalo;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TestFileReader {
    @Test
    public void testReadNumbersFromFile() {
        int result = new FileReader().readNumbersFromFile().length;
        int expected = 20;
        assertEquals(expected, result);
    }

    @Test
    public void testCreateFile() {
        String path = "src/main/resources/simplyFile.txt";
        new FileReader().createFile(path);
        boolean path1 = path.isEmpty();
        assertFalse(path1);
    }
}

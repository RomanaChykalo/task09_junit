package com.chykalo;

import org.junit.Test;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;


public class TestSimpleOperation {
    @Test
    public void testMultiplication() {
        SimpleOperation simpleOperation = new SimpleOperation(new FileReader());
        int value = simpleOperation.multiplication(4, 3);
        assertEquals(20, value, "Success");
    }

    @Test
    @DisplayName("You can't divide by zero.")
    public void testDivideNumbers() {
        ArithmeticException e = assertThrows(ArithmeticException.class,
                () -> {
                    throw new ArithmeticException("You can't divide by zero.");
                });
        assertEquals("You can't divide by zero.", e.getMessage());
    }

    @Test
    public void testGetSumOfAllNumbersBeforeElementNumber() {
        int summ = new SimpleOperation(new FileReader()).getSumOfAllNumbersBeforeElementNumber(5);
        int summ1 = new SimpleOperation(new FileReader()).getSumOfAllNumbersBeforeElementNumber(6);
        boolean b = summ < summ1;
        assumeTrue(b, "Then bigger value of index of an element, then the greater the sum of its predecessors");
    }
}

package com.chykalo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestValidateMethod {
    private int primeNumber;
    private Boolean expectedResult;

    public TestValidateMethod(int primeNumber, boolean expectedResult) {
        this.primeNumber = primeNumber;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(new Object[][]{{2, false}, {99, true}, {1, false}});
    }

    @Test
    public void testValidateMethod() {
        System.out.println("input number is : " + primeNumber);
        Assert.assertEquals(expectedResult,
                new SimpleOperation(new FileReader()).validate(primeNumber));
    }
}
